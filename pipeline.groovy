pipeline {
  agent any
  triggers{
    githubPush()
  }
  stages {
    stage('gitlab') {
          steps {
             echo 'Notify GitLab'
             updateGitlabCommitStatus name: 'build', state: 'pending'
             updateGitlabCommitStatus name: 'build', state: 'success'
          }
       }
    stage('build'){
      steps {
        sh 'docker build -t thetharz/3996-node-app-image .'
      }
    }
    stage('run'){
      steps{
        sh 'docker run -d -p 5000:3000 thetharz/3996-node-app-image'
      }
    }
    stage('status'){
      steps{
        sh 'docker ps'
      }
    }
    
  }
}